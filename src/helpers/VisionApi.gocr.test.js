const GoogleOcr = require('./VisionApi.gocr');
if (process.env.NODE_ENV !== 'production') require('dotenv').config();
jest.setTimeout(60 * 60 * 1000);
describe('Google Vision - OCR', () => {
  it('should extract text from pdf that uploaded on gcs', async () => {
    const bucketName = process.env.BUCKET_NAME;
    const outputDirPrefix = 'test-ocr';
    const fileName = '7efeb690-0c37-4c9c-9209-8fa91f75f78edoc/article2_1_1.pdf';

    const result = await GoogleOcr({
      fileName,
      outputDirPrefix,
      bucketName,
    });
    expect(result).toBeDefined();
  });

  it('should return Error on passing Wrong filePath', async () => {
    const bucketName = 'testedBucket';
    const outputDirPrefix = 'test-ocr';
    const fileName = '7efeb690-0c37-4c9c-9209-8fa91f75f78edoc/article2_1_1.pdf';
    try {
      await GoogleOcr({
        fileName,
        outputDirPrefix,
        bucketName,
      });
    } catch (err) {
      expect(err).toHaveProperty('error');
    }
  });
});
