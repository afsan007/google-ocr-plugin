module.exports = (
	headers = {
		'Access-Control-Allow-Origin': '*',
	},
	{ ...body },
	statusCode = 200
) => {
	if (!Object.keys(body).length)
		throw new Error('No body object Found')
	return {
		headers,
		body: JSON.stringify(body),
		statusCode,
	}
}
