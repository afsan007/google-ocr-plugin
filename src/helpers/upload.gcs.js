'use-strict';

const { Storage } = require('@google-cloud/storage');
const { v4: uuid } = require('uuid');
const fs = require('fs');

const logger = require('./logger');

module.exports = async ({ localFilePath, bucketName }) => {
  logger.info('Start Uploading Pdf', { localFilePath, bucketName });

  // Creates a client
  const storage = new Storage({
    keyFilename: process.env.GOOGLE_SVC,
  });
  const bucket = storage.bucket(bucketName);

  const gcsname = uuid() + localFilePath;
  const files = bucket.file(gcsname);
  return new Promise((res, rej) => {
    fs.createReadStream(localFilePath)
      .pipe(
        files.createWriteStream({
          metadata: {
            contentType: 'application/pdf',
          },
        })
      )
      .on('error', (error) => {
        logger.error('Error Occurred in uploading file', { message: error });
        rej({
          error,
          localFilePath,
          fileUrl: `https://storage.googleapis.com/${bucketName}/${gcsname}`,
        });
      })
      .on('finish', () => {
        logger.info('Pdf File Uploaded Successfully', {
          localFilePath,
          bucketName,
          uploadedFileName: gcsname,
        });
        res({
          fileUrl: `https://storage.googleapis.com/${bucketName}/${gcsname}`,
          uploadedFileName: gcsname,
        });
      });
  });
};
