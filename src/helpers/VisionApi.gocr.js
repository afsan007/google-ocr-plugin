// Imports the Google Cloud client libraries
const logger = require('./logger');
const { ImageAnnotatorClient } = require('@google-cloud/vision').v1;
const { v4: uuid } = require('uuid');
module.exports = async ({ bucketName, fileName, outputDirPrefix }) => {
  logger.info({ message: "Start Extracting Pdf's Text", fileName });

  // Creates a client
  const client = new ImageAnnotatorClient({
    keyFilename: process.env.GOOGLE_SVC,
  });
  const pdfName = fileName.split('/').pop().slice(0, -4);
  const gcsSourceUri = `gs://${bucketName}/${fileName}`;
  const gcsDestinationUri = `gs://${bucketName}/${outputDirPrefix}-${uuid()}-${pdfName}/`;
  const inputConfig = {
    mimeType: 'application/pdf',
    gcsSource: {
      uri: gcsSourceUri,
    },
  };
  const outputConfig = {
    gcsDestination: {
      uri: gcsDestinationUri,
    },
  };
  const features = [{ type: 'DOCUMENT_TEXT_DETECTION' }];
  const request = {
    requests: [
      {
        inputConfig: inputConfig,
        features: features,
        outputConfig: outputConfig,
      },
    ],
  };
  try {
    const [operation] = await client.asyncBatchAnnotateFiles(request);

    const [filesResponse] = await operation.promise();

    const destinationUri = filesResponse.responses[0].outputConfig.gcsDestination.uri;
    logger.info({
      message: 'Your Article has been processed successfully.',
      fileName,
      destinationUri,
    });
    return destinationUri;
  } catch (error) {
    logger.error('Error Occurred in extracting file', { message: error });
    return {
      error,
      fileName,
      bucketName,
    };
  }
};
