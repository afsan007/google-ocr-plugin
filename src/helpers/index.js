module.exports = {
  logger: require('./logger'),
  res: require('./resultForm'),
  UploadToGCS: require('./upload.gcs'),
  GoogleOcr: require('./VisionApi.gocr'),
  validate: require('./validate'),
  eventGenerator: require('./eventGenerator'),
};
