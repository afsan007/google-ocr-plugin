const isValid = require('is-valid-path');
const logger = require('./logger');
const res = require('./resultForm');

module.exports = (event) => {
  // basic validation
  if (!event.queryStringParameters || !event.queryStringParameters.hasOwnProperty('filePath')) {
    logger.error('Please enter file path.');
    return res(null, { message: 'Please enter file path.' }, 400);
  }

  const { filePath } = event.queryStringParameters;

  if (!isValid(filePath)) {
    logger.error('Please enter valid file path.');
    return res(null, { message: 'Please enter valid file path.' }, 400);
  }

  if (!filePath.match(/(.pdf)$/)) {
    logger.error('Please enter valid file, file format should be pdf');
    return res(
      null,
      {
        message: 'Please enter valid file, file format should be pdf',
      },
      400
    );
  }
  return filePath;
};
