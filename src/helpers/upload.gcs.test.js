const UploadToGCS = require('./upload.gcs');
if (process.env.NODE_ENV !== 'production') require('dotenv').config();
jest.setTimeout(60 * 60 * 1000);
describe('Upload On GCS', () => {
  it('should upload article on GCS', async () => {
    const localFilePath = 'doc/test.pdf';
    const bucketName = process.env.BUCKET_NAME;
    const result = await UploadToGCS({
      localFilePath,
      bucketName,
    });
    expect(result).toBeDefined();
    expect(Object.keys(result)).toContain('fileUrl');
    expect(Object.keys(result)).toContain('uploadedFileName');
  });

  it('should return Error on passing Wrong BucketName', async () => {
    const localFilePath = 'doc/test.pdf';
    const bucketName = 'testedBucket';
    await expect(
      UploadToGCS({
        localFilePath,
        bucketName,
      })
    ).rejects.toHaveProperty('error');
  });
});
