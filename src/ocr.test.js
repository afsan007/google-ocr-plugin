const { eventGenerator } = require('./helpers');
const googleOcr = require('./ocr');
jest.setTimeout(60 * 60 * 1000);
describe('Google-OCR-Plugin', () => {
  test('should upload and ocr the article', async () => {
    const filePath = 'doc/article2_1_1.pdf';
    const event = eventGenerator({
      queryStringObject: { filePath },
    });
    const result = await googleOcr.sendArticle(event);
    const body = JSON.parse(result.body);
    expect(result).toBeDefined();
    expect(body.message).toEqual('Your Article has been processed successfully.');
    expect(Object.keys(body)).toContain('localFilePath');
    expect(Object.keys(body)).toContain('fileName');
    expect(Object.keys(body)).toContain('resultFilePath');
  });
  test('should return not Valid File Path', async () => {
    const filePath = '?>>article2_1_1.pdf';
    const event = eventGenerator({
      queryStringObject: { filePath },
    });
    const result = await googleOcr.sendArticle(event);
    const body = JSON.parse(result.body);
    expect(result).toBeDefined();
    expect(body.message).toEqual('Please enter valid file path.');
    expect(result.statusCode).toEqual(400);
  });
  test('should return not found file Path', async () => {
    const event = eventGenerator({});
    const result = await googleOcr.sendArticle(event);
    const body = JSON.parse(result.body);
    expect(body.message).toEqual('Please enter file path.');
    expect(result.statusCode).toEqual(400);
  });

  test('should return error for other format', async () => {
    const filePath = './doc/movie.mov';
    const event = eventGenerator({
      queryStringObject: { filePath },
    });
    const result = await googleOcr.sendArticle(event);
    const body = JSON.parse(result.body);
    expect(body.message).toEqual('Please enter valid file, file format should be pdf');
    expect(result.statusCode).toEqual(400);
  });
});
