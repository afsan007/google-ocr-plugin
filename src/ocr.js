'use strict';
const { UploadToGCS, GoogleOcr, res, validate } = require('./helpers');
if (process.env.NODE_ENV !== 'production') require('dotenv').config();

module.exports.sendArticle = async (event) => {
  const localFilePath = validate(event);
  if (typeof localFilePath !== 'string') return localFilePath;
  const bucketName = process.env.BUCKET_NAME;
  const outputDirPrefix = process.env.OUTPUT_DIR_PREFIX;

  // Step One: store file that user send on GCS
  let uploaded_info;
  try {
    uploaded_info = await UploadToGCS({
      localFilePath,
      bucketName,
    });
  } catch (err) {
    return res(null, { message: err, filePath });
  }

  let resultFilePath;
  try {
    // Step Two: send file to Google OCR
    resultFilePath = await GoogleOcr({
      fileName: uploaded_info.uploadedFileName,
      bucketName,
      outputDirPrefix,
    });
  } catch (err) {
    return res(null, { message: err, filePath });
  }

  return res(
    null,
    {
      message: 'Your Article has been processed successfully.',
      localFilePath: localFilePath,
      fileName: uploaded_info.uploadedFileName,
      resultFilePath,
    },
    200
  );
};
